# To run the music library tracker
```
cd music_library
ruby playlist_runner.rb
```

# To run the tests
```
cd music_library
bundle install
rspec spec/playlist.rb
```
