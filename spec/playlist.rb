require_relative '../src/playlist'
require_relative '../src/errors'
require 'pry'
require 'rspec'

describe Playlist do
  let(:playlist) { Playlist.new }

  describe '#add' do

    context 'an album has been added' do
      before do
        playlist.add("Ride the Lightning", "Metallica")
      end

      it 'adds an album to the playlist' do
        expect(playlist.albums.keys).to eq ["Ride the Lightning"]
      end

      it 'stores title and played data' do
        expect(playlist.albums["Ride the Lightning"][:title]).to eq "Ride the Lightning"
        expect(playlist.albums["Ride the Lightning"][:played]).to be false
      end

      it 'stores the artist' do
        expect(playlist.albums["Ride the Lightning"][:artist].is_a?(Artist)).to be true
        expect(playlist.albums["Ride the Lightning"][:artist].name).to eq "Metallica"
      end
    end
  end

  describe '#play' do
    before do
      playlist.add("Ride the Lightning", "Metallica")
    end

    it 'marks the album as played' do
      expect(playlist.albums["Ride the Lightning"][:played]).to be false
      playlist.play("Ride the Lightning")
      expect(playlist.albums["Ride the Lightning"][:played]).to be true
    end

    it 'raises an error if the track does not exist' do
      expect { playlist.play("Invalid Title") }.to raise_error(InvalidTitleError, "You don't have that album.")
    end
  end

  describe '#show_all' do
    context 'there are albums' do
      let(:expected_output) do
        '"Ride the Lightning" by Metallica (unplayed)' + "\n" +
        '"Licensed to Ill" by Beastie Boys (unplayed)' + "\n" +
        '"Pauls Boutique" by Beastie Boys (played)'
      end

      before do
        playlist.add("Ride the Lightning", "Metallica")
        playlist.add("Licensed to Ill", "Beastie Boys")
        playlist.add("Pauls Boutique", "Beastie Boys")
        playlist.play("Pauls Boutique")
      end

      it 'outputs a list of all albums, including played status' do
        expect(playlist.show_all).to eq expected_output
      end
    end

    context 'there are no albums' do
      it 'informs the user there are no albums to display' do
        expect(playlist.show_all).to eq "No albums found."
      end
    end

    context 'an artist name is given' do
      let(:expected_output) do
        '"Licensed to Ill" by Beastie Boys (unplayed)' + "\n" +
        '"Pauls Boutique" by Beastie Boys (played)'
      end

      before do
        playlist.add("Ride the Lightning", "Metallica")
        playlist.add("Licensed to Ill", "Beastie Boys")
        playlist.add("Pauls Boutique", "Beastie Boys")
        playlist.play("Pauls Boutique")
      end

      it 'outputs a list of all albums by the artist' do
        expect(playlist.show_all("Beastie Boys")).to eq expected_output
      end

      it 'informs the user if there are no albums by the artist in question' do
        expect(playlist.show_all("The Beatles")).to eq "No albums found."
      end
    end
  end

  describe '#show_unplayed' do
    context 'there are unplayed albums' do
      let(:expected_output) do
        '"Ride the Lightning" by Metallica' + "\n" +
        '"Licensed to Ill" by Beastie Boys'
      end

      before do
        playlist.add("Ride the Lightning", "Metallica")
        playlist.add("Licensed to Ill", "Beastie Boys")
        playlist.add("Pauls Boutique", "Beastie Boys")
        playlist.play("Pauls Boutique")
      end

      it 'outputs a list of all unplayed albums' do
        expect(playlist.show_unplayed).to eq expected_output
      end
    end

    context 'there are no unplayed albums' do
      before do
        playlist.add("Pauls Boutique", "Beastie Boys")
        playlist.play("Pauls Boutique")
      end

      it 'informs the user that there are no unplayed albums' do
        expect(playlist.show_unplayed).to eq "No unplayed albums found."
      end
    end

    context 'an artist name is given' do
      let(:expected_output) do
        '"Licensed to Ill" by Beastie Boys' + "\n" +
        '"Check Your Head" by Beastie Boys'
      end

      before do
        playlist.add("Ride the Lightning", "Metallica")
        playlist.add("Licensed to Ill", "Beastie Boys")
        playlist.add("Pauls Boutique", "Beastie Boys")
        playlist.add("Check Your Head", "Beastie Boys")
        playlist.play("Pauls Boutique")
      end

      it 'outputs a list of all unplayed albums by the artist' do
        expect(playlist.show_unplayed("Beastie Boys")).to eq expected_output
      end

      it 'informs the user when there are no unplayed albums by the artist' do
        expect(playlist.show_unplayed("The Beatles")).to eq "No unplayed albums found."
      end
    end
  end
end
