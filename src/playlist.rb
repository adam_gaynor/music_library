require_relative 'artist'

class Playlist
  attr_accessor :artists, :albums
  def initialize
    @artists = {}
    @albums = {}
  end

  def add(title, artist_name)
    # Find the artist or create a new one
    artist = artists[artist_name] || Artist.new(artist_name)
    if albums.has_key?(title)
      raise InvalidTitleError, "You already entered that album."
    else
      # Add the album to our album list as unplayed
      albums[title] = {
        title: title,
        artist: artist,
        played: false
      }
      # Add the album to the artist's list
      artist.add_album(title)
    end
    # Add the artist to our list of artists
    artists[artist_name] = artist
  end

  def play(title)
    raise InvalidTitleError, "You don't have that album." unless albums.has_key?(title)
    albums[title][:played] = true
  end

  def show_all(artist = nil)
    output = albums_by(artist).map { |album| format_album_output(album, show_played_status: true) }.join("\n")
    output.length > 0 ? output : "No albums found."
  end

  def show_unplayed(artist = nil)
    unplayed_albums = albums_by(artist).select { |album| album[:played] == false }
    output = unplayed_albums.map { |album| format_album_output(album, show_played_status: false) }.join("\n")
    output.length > 0 ? output : "No unplayed albums found."
  end

  private

  def format_album_output(album_data, show_played_status:)
    output = "\"#{album_data[:title]}\" by #{album_data[:artist].name}"
    if show_played_status
      output.concat(" (#{album_data[:played] ? "played" : "unplayed"})")
    end

    output
  end

  def albums_by(artist)
    # If we pass no artist name, we want all of them
    return self.albums.values unless artist
    # If an invalid artist is asked for, show no albums
    return [] unless artists[artist]
    # Otherwise, show all albums by the artist
    artists[artist].albums.map { |album_name| self.albums[album_name] }
  end
end
