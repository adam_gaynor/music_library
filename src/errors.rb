class InputError < StandardError; end
class InvalidTitleError < InputError; end
class InvalidArtistError < InputError; end
