require_relative 'src/playlist'
require_relative 'src/errors'
require 'pry'

class PlaylistRunner
  INTRO_TEXT = 'Please enter a command to manage your playlist:' +
    "\n" + ' - add "$title" "$artist": adds an album to the collection with the given title and artist. All albums are unplayed by default' +
    "\n" + ' - play "$title": marks a given album as played' +
    "\n" + ' - show all: displays all of the albums in the collection' +
    "\n" + ' - show unplayed: display all of the albums that are unplayed' +
    "\n" + ' - show all by "$artist": shows all of the albums in the collection by the given artist' +
    "\n" + ' - show unplayed by "$artist": shows the unplayed albums in the collection by the given artist' +
    "\n" + ' - quit: quits the program'

    attr_accessor :playlist, :exit_command_entered

  def initialize
    @playlist = Playlist.new
    @exit_command_entered = false
  end

  def run_program
    print(INTRO_TEXT + "\n\n")
    until exit_command_entered
      begin
        print("Enter Command: ")
        handle_input(gets.chomp)
      rescue InputError => e
        puts e.message
        sleep 1
      end
    end
    puts "Playlist exited."
    exit
  end

  def handle_input(input)
    command = parse_input_command(input)
    case command
    when "add"
      title, artist = parse_add_string(input)
      playlist.add(title, artist)
      puts "Added \"#{title}\" by #{artist}"
    when "play"
      title = parse_play_string(input)
      playlist.play(title)
      puts "You're listening to \"#{title}\""
    when "show all"
      albums_list = playlist.show_all
      puts albums_list
    when "show all by"
      artist = parse_show_string(input)
      albums_list = playlist.show_all(artist)
      puts albums_list
    when "show unplayed"
      albums_list = playlist.show_unplayed
      puts albums_list
    when "show unplayed by"
      artist = parse_show_string(input)
      albums_list = playlist.show_unplayed(artist)
      puts albums_list
    when "quit"
      self.exit_command_entered = true
    else
      raise InputError, "Invalid input command entered: #{command}"
    end
  end

  private

  def parse_input_command(input_string)
    # "show" takes a two or three word command
    if input_string.split.first == "show" && input_string.split[2] == "by"
      # There is a specific artist we are searching for
      command = input_string.split[0..2].join(" ")
    elsif input_string.split.first == "show"
      # It is a general 'show' command
      command = input_string.split[0..1].join(" ")
    else
      command = input_string.split.first
    end

    command
  end

  def parse_add_string(input_string)
    input_terms = input_string.split('"')
    raise InputError, "Invalid <add> input syntax" if input_terms.length != 4
    title = input_terms[1]
    artist = input_terms[3]
    raise InvalidTitleError, "No Title Given" if title.strip.empty?
    raise InvalidArtistError, "No Artist Given" if artist.strip.empty?

    [title, artist]
  end

  def parse_play_string(input_string)
    input_terms = input_string.split('"')
    raise InputError, "Invalid <play> input syntax" if input_terms.length != 2
    title = input_terms[1]
    raise InvalidTitleError, "No Title Given" if title.strip.empty?

    title
  end

  def parse_show_string(input_string)
    input_terms = input_string.split('"')
    raise InputError, "Invalid <show> input syntax" if input_terms.length != 2
    artist = input_terms.last
    raise InvalidArtistError, "No Artist Given" if artist.strip.empty?

    artist
  end
end

PlaylistRunner.new.run_program
